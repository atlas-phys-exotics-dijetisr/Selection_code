#!/usr/bin/python

import os, sys

param=sys.argv

######################################################################
## Check before run
######################################################################

# Change it
PATH = "PATH/"

MC  = True

Sherpa = True

Signal = False#while True, MC must be always True too

Data17 = False

Grid = False

Trijet = False

Single = False #gor gjj sample

Debug = False

Mass=[
 #   "250GeV", 
  #  "350GeV", 
    "450GeV", 
 #   "550GeV", 
]

##################################################################################
#
##################################################################################

command = ""

for mass in Mass:
  command += "cuts_study "
  command += "--mass="
  command += mass
  command += " "

  if MC:
    command += "--isMC=TRUE "
  if not MC:
    command += "--isMC=FALSE "
  
  if Sherpa:
    command += "--isSherpa=TRUE "
  if not Sherpa:
    command += "--isSherpa=FALSE "
  
  if Signal:
    command += "--isSignal=TRUE "
  if not Signal:
    command += "--isSignal=FALSE "
 
  if Data17:
    command += "--is17=TRUE "
  if not Data17:
    command += "--is17=FALSE "

  if Grid:
    command += "--onGrid=TRUE "
  if not Grid:
    command += "--onGrid=FALSE "

  if Trijet:
    command += "--isjjj=TRUE "
  if not Trijet:
    command += "--isjjj=FALSE "

  if Debug:
    command += "--debug=TRUE"

  if Single:
    command += "--isSingleT=TRUE"
  if not Trijet:
    command += "--isSingleT=FALSE"

"

  command += " && "

command = command[:-3]
command += "&"
print command
os.system(command)

