#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include "TLorentzVector.h"
#include <stdlib.h>
#include <algorithm>

#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TBranch.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"
#include <sstream>
#include <iomanip>
using namespace std;
TString PATH;
const int nEtaBins = 90;

// Input tree
TTree *tree;

// Data
TString Data = ""; // 2015 or 2016
bool m_PRW = false;
bool m_doCleaning=false;
bool m_MC = true;
bool m_singleT= false;
int  firstTrig = 0; // HLT_j15 (except for 8LC: HLT_j25)

//------------
// Histograms
//------------

TH1D* h_cutflow;

TH1D* h_jet0_pt;
TH1D* h_jet0_eta;
TH1D* h_jet0_phi;
TH1D* h_jet1_pt;
TH1D* h_jet1_eta;
TH1D* h_jet1_phi;
TH1D* h_jet2_pt;
TH1D* h_jet2_eta;
TH1D* h_jet2_phi;

TH1D*      h_y_23;
TH1D*      h_Asy;
TH1D*      h_Asy_ISR_Zprime;
TH1D*      h_DR;
TH1D*      h_DR_wrt_Z;
TH1D*      h_thrust;
TH1D*      h_pt_diff_ISR_jet1;
TH1D*      h_pt_diff_ISR_jet2;
TH1D*      h_DPhi_ISR_jet1;
TH1D*      h_DPhi_ISR_jet2;
TH1D*      h_DPhi_ISR_Zprime;

TH1D* h_mjj;

//-----------
// Variables
//-----------

Double_t weight;   
Double_t Y;
Double_t Asy;
Double_t Asy_ISR_Zprime;
Double_t DR_closest_jet;
Double_t DR_wrt_Zprime;
Float_t final_thrust; 
Double_t Pt_diff_ISR_jet1;
Double_t Pt_diff_ISR_jet2;
Double_t DPhi_ISR_jet1;
Double_t DPhi_ISR_jet2;
Double_t DPhi_ISR_Zprime;
Double_t Mass;
std::vector<TLorentzVector> Jets;

//configuration variables
bool m_debug;
bool m_doData;
bool m_signal;
bool m_onGrid;
bool m_isjjj;
bool m_Sherpa;
bool m_data17;
std::string m_mass;

// float cutValue;
int m_eventCounter;     //!

TString m_name;
float m_mcEventWeight;  //!

float m_ht; //!
int m_NPV; //!
float m_actualInteractionsPerCrossing; //!
float m_averageInteractionsPerCrossing; //!                                                                               
int m_runNumber; //!
Long64_t  m_eventNumber; //!
int m_lumiBlock; //!

float m_weight; //!
float m_weight_xs; //!
float m_weight_pileup;    //!
    
std::vector<float>* m_jet_pt; //!
std::vector<float>* m_jet_eta; //!
std::vector<float>* m_jet_phi; //!
std::vector<float>* m_jet_E; //!
std::vector<float>* m_jet_Timing; //!
std::vector<float>* m_jet_GhostMuonSegmentCount; //!
std::vector<int>*   m_jet_clean_passLooseBad; //!
std::vector<std::string>* m_passedTriggers; //!
std::vector<float>*      m_ph_pt;
std::vector<float>*      m_ph_phi;
std::vector<float>*      m_ph_eta;

//functions

void Initialize();
void Enable_and_Set_Branches(TChain & tree);
void BookHistograms();
bool passTrig(TString trigName);
void GetListOfRootFiles ( std::vector< TString> & Files);    
