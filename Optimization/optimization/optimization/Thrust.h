 #ifndef WRAP_THRUST_HH
#define WRAP_THRUST_HH

// ================================================================
// 
//   Thrust.hh
//
//        Compute the vector which maximizes "thrust" with a
//        collection of 3-momenta.  For the definition of
//        "thrust", see CDF7402.
//
//        Ben Brau, Antonio Boveia, UC Santa Barbara
//
// ================================================================

#include <vector>
#include <TVector3.h>

class
Thrust
{
public:
  typedef std::vector<TVector3>::iterator       iterator;
  typedef std::vector<TVector3>::const_iterator const_iterator;

  Thrust();
  Thrust( const std::vector<TVector3>& plist );

  float thrust( const TVector3& thrustAxis ) const;
  TVector3 thrustAxis();

  bool operator()( const TVector3& x , const TVector3& y ) { return y.Mag() < x.Mag(); }
protected:
  short epsilon( const float& x ) const { return x > 0 ?  1 :  -1; };
  TVector3 thrustGuess( const TVector3& thrustJ ) const;
  std::vector<TVector3> _plist;
  TVector3 _thrustAxis;
  TVector3 _pSum;
  float    _thrust;
  bool     _debug;
};


#endif // #ifndef WRAP_THRUST_HH
