/* **********************************************************************\
 *                                                                      *
 *  #   Name:   cuts_study        	                                *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 29/11/17 First version             R. Devesa (mdevesa@cern.ch)    *
 *  2 19/12/17 Second version            R. Devesa (mdevesa@cern.ch)    * 
 *  3 03/01/18 Third version             F. Daneri (mdaneri@cern.ch)    * 
\************************************************************************/

#include "../optimization/cuts_study.h"
#include "../Root/Thrust.cxx"

int main(int argc, char* argv[]) {
 
  std::string inputFileUser = "";
  m_mass = "";

  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }

    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_debug= true;
      if (v[1].find("FALSE") != std::string::npos) m_debug= false;
    }
    if ( opt.find("--is17=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_data17 = true;
      if (v[1].find("FALSE") != std::string::npos) m_data17 = false;
    }
    if ( opt.find("--isMC=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_MC= true;
      if (v[1].find("FALSE") != std::string::npos) m_MC= false;
    }
    if ( opt.find("--isSignal=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_signal= true;
      if (v[1].find("FALSE") != std::string::npos) m_signal= false;
    }
    if ( opt.find("--onGrid=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_onGrid= true;
      if (v[1].find("FALSE") != std::string::npos) m_onGrid= false;
    }
    if ( opt.find("--isjjj=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_isjjj= true;
      if (v[1].find("FALSE") != std::string::npos) m_isjjj= false;
    }
   if ( opt.find("--isSingleT=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_singleT= true;
      if (v[1].find("FALSE") != std::string::npos) m_singleT= false;
    }
    if ( opt.find("--isSherpa=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) m_Sherpa= true;
      if (v[1].find("FALSE") != std::string::npos) m_Sherpa= false;
    }
    if ( opt.find("--mass=") != std::string::npos){
      if (v[1].find("250GeV") != std::string::npos) m_mass = "250GeV";
      if (v[1].find("350GeV") != std::string::npos) m_mass = "350GeV";
      if (v[1].find("450GeV") != std::string::npos) m_mass = "450GeV";
      if (v[1].find("550GeV") != std::string::npos) m_mass = "550GeV";
    }

  }//End: Loop over input options
 
  TStopwatch timer; 
  timer.Start(); 
  //gROOT->Reset(); 
  //gROOT->ProcessLine(".L loader.C+"); 
  
  Initialize();
  
  //-----------------------
  // Getting File
  //-----------------------
   
  std::vector<TString> Files;
  GetListOfRootFiles(Files);
  
  std::cout << "Files.size()"<<Files.size() << std::endl;
  
  //-----------------------
  // Getting Tree
  //-----------------------
  
  TChain tree("outTree");
  
  for (unsigned int i_file = 0; i_file != Files.size(); ++i_file) {
    tree.Add(Files.at(i_file).Data());
  }
    
  //---------------------------
  // Enable necessary branches
  //---------------------------
  
  Enable_and_Set_Branches(tree);
    
  // Number of events
  Long64_t entries = tree.GetEntries();
    
  if(m_debug) std::cout << "entries:"<< entries << std::endl;
  
  //------------------
  // Loop over entries
  //------------------
  if(m_debug) std::cout << "Loop over entries" << std::endl;
  TTree *tree_ = new TTree("Tree_","discriminants");
  
  tree_->Branch("Weight"             ,&weight             ,"Weight/double");
  tree_->Branch("Y"                  ,&Y                  ,"Y/double");
  tree_->Branch("Asy"                ,&Asy                ,"Asy/double");
  tree_->Branch("DR_closest_jet"     ,&DR_closest_jet     ,"DR_closest_jet/double");
  tree_->Branch("DR_wrt_Zprime"      ,&DR_wrt_Zprime      ,"DR_wrt_Zprime/double");
  //tree_->Branch("Thrust"             ,&final_thrust       ,"Thrust/float"); 
  //tree_->Branch("Asy_ISR_Zprime"     ,&Asy_ISR_Zprime     ,"Asy_ISR_Zprime/double");
  //tree_->Branch("Pt_diff_ISR_jet1"   ,&Pt_diff_ISR_jet1   ,"Pt_diff_ISR_jet1/double");
  //tree_->Branch("Pt_diff_ISR_jet2"   ,&Pt_diff_ISR_jet2   ,"Pt_diff_ISR_jet2/double");
  //tree_->Branch("DPhi_ISR_jet1"      ,&DPhi_ISR_jet1      ,"DPhi_ISR_jet1/double");
  //tree_->Branch("DPhi_ISR_jet2"      ,&DPhi_ISR_jet2      ,"DPhi_ISR_jet2/double");
  //tree_->Branch("DPhi_ISR_Zprime"    ,&DPhi_ISR_Zprime    ,"DPhi_ISR_Zprime/double");
  tree_->Branch("Mass"               ,&Mass               ,"Mass/double");

  TTree *tree_nocuts = new TTree("Tree_noCuts","discriminants");
  
  tree_nocuts->Branch("Weight"             ,&weight             ,"Weight/double");
  tree_nocuts->Branch("Y"                  ,&Y                  ,"Y/double");
  tree_nocuts->Branch("Asy"                ,&Asy                ,"Asy/double");
  tree_nocuts->Branch("DR_closest_jet"     ,&DR_closest_jet     ,"DR_closest_jet/double");
  tree_nocuts->Branch("DR_wrt_Zprime"      ,&DR_wrt_Zprime      ,"DR_wrt_Zprime/double");
  //tree_nocuts->Branch("Thrust"             ,&final_thrust       ,"Thrust/float"); 
  //tree_nocuts->Branch("Asy_ISR_Zprime"     ,&Asy_ISR_Zprime     ,"Asy_ISR_Zprime/double");
  //tree_nocuts->Branch("Pt_diff_ISR_jet1"   ,&Pt_diff_ISR_jet1   ,"Pt_diff_ISR_jet1/double");
  //tree_nocuts->Branch("Pt_diff_ISR_jet2"   ,&Pt_diff_ISR_jet2   ,"Pt_diff_ISR_jet2/double");
  //tree_nocuts->Branch("DPhi_ISR_jet1"      ,&DPhi_ISR_jet1      ,"DPhi_ISR_jet1/double");
  //tree_nocuts->Branch("DPhi_ISR_jet2"      ,&DPhi_ISR_jet2      ,"DPhi_ISR_jet2/double");
  //tree_nocuts->Branch("DPhi_ISR_Zprime"    ,&DPhi_ISR_Zprime    ,"DPhi_ISR_Zprime/double");
  tree_nocuts->Branch("Mass"               ,&Mass               ,"Mass/double");

  //Setup trigger
    TString trigger_name; 
    float trigger_cut_xat99;
    if (m_isjjj){
      if (m_data17)      {trigger_name = "HLT_j225_gsc400_boffperf_split"; trigger_cut_xat99 = 430; }
      else if (!m_data17){trigger_name = "HLT_j380"                      ; trigger_cut_xat99 = 430; }
    }else{
      if( m_singleT){ trigger_name = "HLT_g140_loose"; trigger_cut_xat99 = 150;}
       if (!m_singleT){ trigger_name = "HLT_g75_tight_3j50noL1_L1EM22VHI"; trigger_cut_xat99 = 85; }
    }
   
   std::cout << "trigger_name -->"<<trigger_name<< std::endl; 
  
  for (Long64_t entry=0; entry < entries; ++entry) {
    
    bool should_skip = false; // Jet Cleaning
    
    tree.GetEntry(entry);
    h_cutflow->Fill(0); // Total Number of events
    h_cutflow->GetXaxis()-> SetBinLabel(1,"AllEvent") ;
    //Show status
    if(entry % 1000000 == 0) std::cout << "Entry = " << entry << " of " << entries << std::endl;

    //---------------------
    // Weight (normalized to 1 inverse pb)
    //---------------------
    double weight_ntuple = 1.;
    //MC
    if(m_MC) weight_ntuple = m_weight;
    //Data
    if(!m_MC) weight_ntuple = 1;

    weight = weight_ntuple/entries;
    
    //-------------
    //   Triggers
    //-------------
//     for (int j=0 ; j < m_passedTriggers->size();++j){
//       //if (m_passedTriggers->at(j)=="HLT_3j200"){      
// std::cout << "m_passedTriggers: " << m_passedTriggers->at(j) << std::endl; 
//       //}
//     }
    /// Loop over reco fat jets
    Jets.clear();
    for (unsigned int ii = 0; ii != m_jet_pt->size(); ++ii) {
	
        double pt  = m_jet_pt ->at(ii);
	double eta = m_jet_eta->at(ii);
	double phi = m_jet_phi->at(ii);
	double E   = m_jet_E  ->at(ii);

	TLorentzVector j;
        j.SetPtEtaPhiE(pt,eta,phi,E);
        Jets.push_back(j);
	
    } //END: Loop over jets
    
    if(Jets.size()==0){continue;}
    
    h_cutflow->Fill(1); // Total Number of events
    h_cutflow->GetXaxis()-> SetBinLabel(2,"nozerojets") ;
    
    if(m_passedTriggers->size()==0){continue; }

    if (m_isjjj){if(Jets.size()<3){continue;}}
    else {if(Jets.size()<2 || m_ph_pt->size()<1){continue;}}
    
    h_cutflow->GetXaxis()-> SetBinLabel(3,"gamma_jets_cuts") ;
    h_cutflow->Fill(2); 
    
    /////////////
    // Trigger //
    /////////////
    if(m_debug) std::cout << "Trigger..."<< std::endl; 

    if(!passTrig(trigger_name)){continue;}
    
    h_cutflow->GetXaxis()-> SetBinLabel(4,"trigger cut") ;
    h_cutflow->Fill(3);

    //The number of the jet depends of the sample (m_isjjj / !m_isjjj)
    int jet1; int jet2;
    if (m_isjjj){jet1=1;jet2=2;}
    else{jet1=0;jet2=1;}
    
    ////////////
    // xat99% //
    ////////////
    if(m_debug) std::cout << "xat99 cut"<< std::endl; 
    float ptcut;
    if (m_isjjj){ptcut=Jets.at(0).Pt();}
    else{ptcut=m_ph_pt->at(0);}
      
    if(ptcut<trigger_cut_xat99){continue;}
    //if(Jets.at(1).Pt()<65){continue;}//only for combined triggers, cut on second jet pt    
 
    h_cutflow->GetXaxis()-> SetBinLabel(5,"xat99") ;
    h_cutflow->Fill(4); 
   
    ////////////////////////////
    // Filling control plots  //
    ////////////////////////////

    if(m_debug) std::cout << "Filling control plots"<< std::endl;  
    if (m_isjjj){
      h_jet0_pt ->Fill(Jets.at(0).Pt() , weight);    h_jet1_pt ->Fill(Jets.at(1).Pt() , weight);      h_jet2_pt ->Fill(Jets.at(2).Pt() , weight);
      h_jet0_eta->Fill(Jets.at(0).Eta(), weight);    h_jet1_eta->Fill(Jets.at(1).Eta(), weight);      h_jet2_eta->Fill(Jets.at(2).Eta(), weight);
      h_jet0_phi->Fill(Jets.at(0).Phi(), weight);    h_jet1_phi->Fill(Jets.at(1).Phi(), weight);      h_jet2_phi->Fill(Jets.at(2).Phi(), weight);
    }else{
      h_jet1_pt ->Fill(Jets.at(0).Pt() , weight);      h_jet2_pt ->Fill(Jets.at(1).Pt() , weight);
      h_jet1_eta->Fill(Jets.at(0).Eta(), weight);      h_jet2_eta->Fill(Jets.at(1).Eta(), weight);
      h_jet1_phi->Fill(Jets.at(0).Phi(), weight);      h_jet2_phi->Fill(Jets.at(1).Phi(), weight); 
    }
    
    /////////
    //  y* //
    /////////
  
    double y_j2= Jets.at(jet1).Rapidity();
    double y_j3= Jets.at(jet2).Rapidity(); 

    Y = (sqrt((y_j2-y_j3)*(y_j2-y_j3)))/2;
    if(m_debug) std::cout << "y*:"<<Y << std::endl;  
    
    ///////////////
    // asymmetry //
    ///////////////

    double A = sqrt((Jets.at(jet1).Pt())*(Jets.at(jet1).Pt()));
    double B = sqrt((Jets.at(jet2).Pt())*(Jets.at(jet2).Pt()));
    Asy = (A-B)/(A+B);

    double C;
    if(m_isjjj){C = sqrt((Jets.at(0).Pt())*(Jets.at(0).Pt()));} 
    if(!m_isjjj){C = sqrt((m_ph_pt->at(0))*(m_ph_pt->at(0)));}
    double Zprime_Pt = (Jets.at(jet1) + Jets.at(jet2)).Pt();
    double D = sqrt(Zprime_Pt*Zprime_Pt);
    Asy_ISR_Zprime = (C-D)/(C+D);
   
    ///////////////////
    // Pt difference //
    ///////////////////
    
    double ISR_pt;
    if(m_isjjj){ISR_pt=Jets.at(0).Pt();}
    if(!m_isjjj){ISR_pt=m_ph_pt->at(0);} 
    
    Pt_diff_ISR_jet1 = ISR_pt/Jets.at(jet1).Pt();
    Pt_diff_ISR_jet2 = ISR_pt/Jets.at(jet2).Pt();

    ////////////
    // Thrust //
    ////////////
    
    if(m_isjjj){
      std::vector<TVector3> ptlist;
      ptlist.reserve(3);//3 jets
      for(int j = 0; j < 3 ; j++){
        TVector3 pt(((Jets.at(j)).Px()),((Jets.at(j)).Py()),0);
        ptlist.push_back(pt);
    }
      std::vector<TVector3> const& pslist = ptlist;
      Thrust thrust = Thrust(pslist);
      TVector3 myThrust = thrust.thrustAxis();
      final_thrust = myThrust.Mag();//esto no está bien!
    }
    //else{float final_thrust = 0;}
    //std::cout << "Thrust value: " << final_thrust << std::endl;

    ////////
    // DR //
    ////////

    double Dr1;
    double Dr2;

    ///Dr wrt Z prime 

    if(!m_isjjj){

      TLorentzVector ph;
      ph.SetPtEtaPhiE(m_ph_pt->at(0),m_ph_eta->at(0),m_ph_phi->at(0),m_ph_pt->at(0));

      Dr1 = ph.DeltaR(Jets.at(jet1));
      Dr2 = ph.DeltaR(Jets.at(jet2));
      DR_wrt_Zprime = ph.DeltaR( (Jets.at(jet1) + Jets.at(jet2)) );
    }else{
      Dr1 = Jets.at(0).DeltaR(Jets.at(jet1));
      Dr2 = Jets.at(0).DeltaR(Jets.at(jet2));
      DR_wrt_Zprime = Jets.at(0).DeltaR( (Jets.at(jet1) + Jets.at(jet2)) );
    }
    if (Dr1<Dr2){DR_closest_jet = Dr1;}
    else {DR_closest_jet = Dr2;}

    //////////
    // DPhi //
    //////////

    if(!m_isjjj){

      TLorentzVector ph;
      ph.SetPtEtaPhiE(m_ph_pt->at(0),m_ph_eta->at(0),m_ph_phi->at(0),m_ph_pt->at(0));

      DPhi_ISR_jet1 = ph.DeltaPhi(Jets.at(jet1));
      DPhi_ISR_jet2 = ph.DeltaPhi(Jets.at(jet2));
      DPhi_ISR_Zprime = ph.DeltaPhi( (Jets.at(jet1) + Jets.at(jet2)) );
    }else{
      DPhi_ISR_jet1 = Jets.at(0).DeltaPhi(Jets.at(jet1));
      DPhi_ISR_jet2 = Jets.at(0).DeltaPhi(Jets.at(jet2));
      DPhi_ISR_Zprime = Jets.at(0).DeltaPhi( (Jets.at(jet1) + Jets.at(jet2)) );
    }

    /////////////////////////
    // Filling histograms  //
    /////////////////////////
   
     if(m_debug) std::cout << "Filling histos"<< std::endl;
    //1D
    Mass = (Jets.at(jet1) + Jets.at(jet2)).M();
    double min_mass = 0;
    double max_mass = 0;

    if(m_mass=="250GeV"){min_mass=200; max_mass=300;}
    if(m_mass=="350GeV"){min_mass=300; max_mass=400;}
    if(m_mass=="450GeV"){min_mass=400; max_mass=500;}
    if(m_mass=="550GeV"){min_mass=500; max_mass=600;}
    
    //std::cout << "min mass: " << min_mass << "       max mass: " << max_mass << std::endl;

    if(Mass > min_mass && Mass < max_mass){
      h_y_23->Fill(Y,weight);
      h_Asy->Fill(Asy,weight);
      h_DR->Fill(DR_closest_jet,weight);
      h_DR_wrt_Z->Fill(DR_wrt_Zprime,weight);
      h_thrust->Fill(final_thrust,weight);
      h_Asy_ISR_Zprime->Fill(Asy_ISR_Zprime,weight);
      h_pt_diff_ISR_jet1->Fill(Pt_diff_ISR_jet1,weight);
      h_pt_diff_ISR_jet2->Fill(Pt_diff_ISR_jet2,weight);
      h_DPhi_ISR_jet1->Fill(DPhi_ISR_jet1,weight);
      h_DPhi_ISR_jet2->Fill(DPhi_ISR_jet2,weight);
      h_DPhi_ISR_Zprime->Fill(DPhi_ISR_Zprime,weight);
        
      if(m_debug) std::cout << "Filling tree"<< std::endl;
      //Fill tree
      tree_->Fill();
    }

     //if(ISR_pt<150 && Jets.at(jet2).Pt()>65){h_mjj->Fill(Mass,weight);}
     h_mjj->Fill(Mass,weight);
     
     if(m_debug) std::cout << "Filling tree with out cuts"<< std::endl;
     tree_nocuts->Fill(); 

    //------------------------------ 
    // Cleaning values
    //------------------------------
    
    if(m_debug) std::cout << "Cleaning vectors" << std::endl;

    m_runNumber = 999;
    m_eventNumber = 999;
    m_lumiBlock = 999;
    m_NPV = 999;
    m_weight_pileup = 999;
    m_weight = 999.;
    m_weight_xs = 999.;


    // Cleaning vectors
    m_jet_clean_passLooseBad->clear();
    m_jet_pt->clear();
    m_jet_eta->clear();
    m_jet_phi->clear();
    m_jet_E->clear();
    m_passedTriggers->clear();
    
    if (!m_isjjj){
      m_ph_pt ->clear();
    }
    
    
    if(m_debug) std::cout << "Entry analyzed" << std::endl;
    if(m_debug) std::cout << "Entry number:"<< entry << std::endl;

  } // End: Loop over entries

  //------------------------------------
  // Saving Histogramas into a ROOT file
  //------------------------------------
  
  std::cout << "Saving plots into a ROOT file..." << std::endl;

  // Output file name
  TString outputfile("");
  
  if(m_MC && m_signal)         {outputfile += "cuts_Signal_";}
  else if(m_MC && !m_signal && m_Sherpa)    {outputfile += "cuts_MC_";}
  else if(m_MC && !m_signal && !m_Sherpa)   {outputfile += "cuts_MC_Pythia_";}
  else if (!m_MC && m_data17 ) {outputfile += "cuts_Data17_";}
  else if (!m_MC && !m_data17 ){outputfile += "cuts_Data16_";}
 
  outputfile += m_mass;
  if(m_isjjj){outputfile += "_trijet_HLT_j380";}
  else{
    if(m_singleT){outputfile += "_gamma_dijet_HLT_g140_loose";}
    else if(!m_singleT){outputfile += "_gamma_dijet_HLT_g75_tight_3j50noL1_L1EM22VHI";}
  outputfile += ".root";
 }
  // Opening output file
  TFile* tout = new TFile(outputfile,"recreate");
  std::cout << "output file = " << outputfile << std::endl;
  tout->cd();
  tree_->Write();
  tree_nocuts->Write();
 
  //Writing histograms
  h_cutflow      ->Write();
  
  h_y_23             ->Write();
  h_Asy              ->Write();
  h_DR               ->Write();
  h_DR_wrt_Z         ->Write();
  h_thrust           ->Write();
  h_Asy_ISR_Zprime   ->Write();
  h_pt_diff_ISR_jet1 ->Write();
  h_pt_diff_ISR_jet2 ->Write();
  h_DPhi_ISR_jet1    ->Write();
  h_DPhi_ISR_jet2    ->Write();
  h_DPhi_ISR_Zprime  ->Write();

  //control plots
  h_jet0_pt  ->Write();
  h_jet0_eta ->Write();
  h_jet0_phi ->Write();
  h_jet1_pt  ->Write();
  h_jet1_eta ->Write();
  h_jet1_phi ->Write();
  h_jet2_pt  ->Write();
  h_jet2_eta ->Write();
  h_jet2_phi ->Write();
  h_mjj      ->Write();

  tout->Close();

  return 0;

}




/*
 *
 *
 * Personal Functions
 *
 *
 *
 * */



bool passTrig(TString trigName){
  return (find(m_passedTriggers->begin(), m_passedTriggers->end(), trigName ) != m_passedTriggers->end());
}

void Enable_and_Set_Branches(TChain & tree){

  tree.SetBranchStatus("*",0); //disable all branches

  tree.SetBranchStatus  ("runNumber",    1);
  tree.SetBranchAddress ("runNumber",    &m_runNumber);

  tree.SetBranchStatus  ("eventNumber",    1);
  tree.SetBranchAddress ("eventNumber",    &m_eventNumber);

  tree.SetBranchStatus  ("lumiBlock",    1);
  tree.SetBranchAddress ("lumiBlock",    &m_lumiBlock);

  tree.SetBranchStatus  ("NPV",    1);
  tree.SetBranchAddress ("NPV",    &m_NPV);

  tree.SetBranchStatus  ("weight_pileup", 1);
  tree.SetBranchAddress ("weight_pileup", &m_weight_pileup);

  tree.SetBranchStatus  ("jet_clean_passLooseBad", 1);
  tree.SetBranchAddress ("jet_clean_passLooseBad", &m_jet_clean_passLooseBad);

  tree.SetBranchStatus  ("passedTriggers", 1);
  tree.SetBranchAddress ("passedTriggers", &m_passedTriggers);

  tree.SetBranchStatus  ("weight", 1);
  tree.SetBranchAddress ("weight", &m_weight);

  tree.SetBranchStatus  ("weight_xs", 1);
  tree.SetBranchAddress ("weight_xs", &m_weight_xs);


  tree.SetBranchStatus  ("jet_pt", 1);
  tree.SetBranchAddress ("jet_pt", &m_jet_pt);

  tree.SetBranchStatus  ("jet_eta", 1);
  tree.SetBranchAddress ("jet_eta", &m_jet_eta);

  tree.SetBranchStatus  ("jet_phi", 1);
  tree.SetBranchAddress ("jet_phi", &m_jet_phi);

  tree.SetBranchStatus  ("jet_E", 1);
  tree.SetBranchAddress ("jet_E", &m_jet_E);

  if (!m_isjjj){
    tree.SetBranchStatus  ("ph_pt", 1); 
    tree.SetBranchAddress ("ph_pt", &m_ph_pt);

    tree.SetBranchStatus  ("ph_phi", 1);
    tree.SetBranchAddress ("ph_phi", &m_ph_phi);

    tree.SetBranchStatus  ("ph_eta", 1);
    tree.SetBranchAddress ("ph_eta", &m_ph_eta); 
  }
  
}//END: EnableBranches()



//****************************************************************************+
void Initialize()
{

    int nBins = 800;    int nBins_eta = 600 ;  int nBins_phi = 600;
    float min = 0;      float min_eta = -3  ;  float min_phi = 0;
    float max = 800;    float max_eta = 3   ;  float max_phi = 6;

    h_cutflow = new TH1D("Cutflow","",11,-0.5,10.5);    h_cutflow->Sumw2();
    // List of discriminants 
    h_y_23         =  new TH1D("y*"    , "y^{*}_{jj}"                                        ,  90,  0,  3)    ;    h_y_23         -> Sumw2();
    h_Asy          =  new TH1D("Asy"   ,"(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)",  120, 0,  1)    ;    h_Asy          -> Sumw2();
    h_DR           =  new TH1D("DeltaR"    , "#DeltaR_{ISR,close-j}"                         ,  100, 0,  5)    ;    h_DR           -> Sumw2();
    h_DR_wrt_Z     =  new TH1D("DeltaR_wrt_Z"    , "#DeltaR_{ISR,j+j}"                       ,  100, 0,  5)    ;    h_DR_wrt_Z     -> Sumw2();
    h_thrust       =  new TH1D("Thrust"    , "Thrust"                                        ,  150, 0.5,  1.2)  ;    h_thrust       -> Sumw2();
    h_Asy_ISR_Zprime  =  new TH1D("Asy_ISR_Zprime"   ,"(|p_{T}^{ISR}|-|p_{T}^{Reso}|)/(|p_{T}^{ISR}|+|p_{T}^{Reco}|)",  200, -1,  1)    ;    h_Asy_ISR_Zprime       -> Sumw2();
    h_pt_diff_ISR_jet1  =  new TH1D("Pt_diff_ISR_jet1"    , "pt difference ISR/jet1"         ,  300,  0,  5)   ;    h_pt_diff_ISR_jet1     -> Sumw2();
    h_pt_diff_ISR_jet2  =  new TH1D("Pt_diff_ISR_jet2"    , "pt difference ISR/jet2"         ,  500,  0,  30)   ;    h_pt_diff_ISR_jet2     -> Sumw2();
    h_DPhi_ISR_jet1   =  new TH1D("DPhi_ISR_jet1"    , "#DeltaPhi{ISR,jet1}"                 ,  400,  -3.5,  3.5)   ;  h_DPhi_ISR_jet1     -> Sumw2();
    h_DPhi_ISR_jet2   =  new TH1D("DPhi_ISR_jet2"    , "#DeltaPhi{ISR,jet2}"                 ,  400,  -3.5,  3.5)   ;  h_DPhi_ISR_jet2     -> Sumw2();
    h_DPhi_ISR_Zprime   =  new TH1D("DPhi_ISR_Zprime"    , "#DeltaPhi{ISR,Zprime}"           ,  400,  -3.5,  3.5)   ;  h_DPhi_ISR_Zprime   -> Sumw2();
     
    //Control Plots
    h_jet0_pt   =  new TH1D("jet0_pt",  "jet0_pt" ,  nBins, min, max);                   h_jet0_pt    ->Sumw2();
    h_jet0_eta  =  new TH1D("jet0_eta", "jet0_eta",  nBins_eta, min_eta, max_eta);       h_jet0_eta   ->Sumw2();
    h_jet0_phi  =  new TH1D("jet0_phi", "jet0_phi",  nBins_phi, min_phi, max_phi);       h_jet0_phi   ->Sumw2();
    h_jet1_pt   =  new TH1D("jet1_pt",  "jet1_pt" ,  nBins, min, max);                   h_jet1_pt    ->Sumw2();
    h_jet1_eta  =  new TH1D("jet1_eta", "jet1_eta",  nBins_eta, min_eta, max_eta);       h_jet1_eta   ->Sumw2();
    h_jet1_phi  =  new TH1D("jet1_phi", "jet1_phi",  nBins_phi, min_phi, max_phi);       h_jet1_phi   ->Sumw2();
    h_jet2_pt   =  new TH1D("jet2_pt" , "jet2_pt" ,  nBins, min, max);                   h_jet2_pt    ->Sumw2();
    h_jet2_eta  =  new TH1D("jet2_eta", "jet2_eta",  nBins_eta, min_eta, max_eta);       h_jet2_eta   ->Sumw2();
    h_jet2_phi  =  new TH1D("jet2_phi", "jet2_phi",  nBins_phi, min_phi, max_phi);       h_jet2_phi   ->Sumw2(); 
    

    h_mjj  =  new TH1D("mjj", "mjj", 50, 0, 2000); h_mjj->Sumw2(); 
    
}//END: Initialize()




void GetListOfRootFiles ( std::vector< TString> & Files)
{

  TString pathFiles="";
  if(!m_MC && !m_data17 && !m_signal) {

        if (m_onGrid==false){ pathFiles="/eos/atlas/user/m/mdevesa/Data16/";}
       std::cout << "Sample data 16" << std::endl;
      
      /// period A
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodA.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464914._000027.tree.root");

      /// period B
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464916._000035.tree.root");

     //       /// Perriod C

      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000036.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000037.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000038.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000039.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000040.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000041.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000042.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000043.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000044.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000045.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000046.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000047.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000048.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000049.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000050.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000051.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000052.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000053.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000054.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000055.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000056.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000057.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000058.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodC.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464917._000059.tree.root");

      //     ///Periodo D

      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000036.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000037.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000038.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000039.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000040.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000041.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000042.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000043.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000044.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000045.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000046.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000047.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000048.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000049.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000050.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000051.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000052.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000053.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000054.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000055.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000056.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000057.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000058.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000059.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000060.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000061.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000062.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000063.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000064.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000065.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000066.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000067.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000068.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000069.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000070.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000071.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000072.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000073.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000074.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000075.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000076.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000077.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000078.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000079.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000080.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000081.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodD.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464918._000082.tree.root");

      //       /// PeriodE
      // 
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodE.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464919._000023.tree.root");

      // 	  ///periodF

      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000036.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000037.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000038.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000039.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000041.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000042.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000043.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000044.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000045.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodF.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464920._000046.tree.root");

      //     ///Periodo G
      // 
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000036.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000037.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000038.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000039.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000040.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000041.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000042.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000043.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000044.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000045.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000046.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000047.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000048.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000049.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000050.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000051.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodG.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464921._000052.tree.root");

      //     //period I
	    
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000036.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000037.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000038.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000039.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000040.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000041.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000042.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000043.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000044.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000045.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000046.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000047.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000048.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000049.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000050.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000051.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000052.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000053.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000054.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000055.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000056.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000057.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000058.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000059.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000060.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000061.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000062.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000063.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000064.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000065.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000066.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000067.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000068.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000069.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000071.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000072.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000073.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000074.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000075.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000076.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000077.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000078.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000079.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000081.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000084.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000085.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodI.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464922._000086.tree.root");

      // 	///Periodo K

      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000016.tree.root.2");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodK.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464925._000036.tree.root"); 

      // 	///periodo L
      // 
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000001.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000002.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000003.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000004.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000005.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000006.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000007.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000008.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000009.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000010.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000011.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000012.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000013.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000014.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000015.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000016.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000017.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000018.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000019.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000020.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000021.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000022.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000023.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000024.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000025.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000026.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000027.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000028.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000029.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000030.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000031.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000032.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000033.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000034.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000035.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000036.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000037.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000038.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000039.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000040.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000041.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000042.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000043.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000044.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000045.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000046.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000047.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000048.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000049.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000051.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000052.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000053.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000054.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000055.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000056.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000057.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000058.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000059.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000060.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000061.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000062.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000063.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000064.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000065.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000066.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000067.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000068.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000069.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000070.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000071.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000072.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000073.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000074.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000075.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000076.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000077.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000078.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000079.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000080.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000081.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000082.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000083.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000084.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000085.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000086.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000087.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000088.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000089.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000090.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000091.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000092.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000093.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000094.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000095.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000096.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000097.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000098.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000099.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000100.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000101.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000102.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000103.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000104.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000105.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000106.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000107.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000108.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000109.tree.root");
      Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodL.physics_Main.trijet.2017.10.28_tree.root/user.kpachal.12464926._000111.tree.root");							                                                                                                 
    
  }if(!m_MC && m_data17 && !m_signal) {
    
      if (m_onGrid==false){ pathFiles="/2/ro/Data17/"/*"/eos/atlas/user/m/mdaneri/Data17/"*/;}
    
 
      std::cout << "Sample data 17" << std::endl;
      
      
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00325713.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578917._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00325713.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578917._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00325713.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578917._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00325789.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578918._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00325790.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578920._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326439.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578921._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326446.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578923._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326446.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578923._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326446.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578923._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326468.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578924._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326551.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578926._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326551.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578926._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326551.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578926._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326551.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578926._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326657.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578927._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326695.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578928._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326695.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578928._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326695.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578928._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326834.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578929._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326870.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578930._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326923.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578932._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326923.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578932._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326923.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578932._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326945.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578933._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326945.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578933._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326945.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578933._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00326945.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578933._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327057.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578936._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327057.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578936._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327057.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578936._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327057.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578936._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327057.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578936._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327103.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578937._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327265.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578938._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327265.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578938._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327265.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578938._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327265.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578938._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327265.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578938._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327342.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578939._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327342.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578939._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327342.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578939._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327342.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578939._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327342.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578939._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327342.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578939._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327490.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578942._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327490.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578942._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327490.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578942._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327490.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578942._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327490.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578942._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327490.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578942._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327582.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578943._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327636.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578944._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327636.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578944._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327636.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578944._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578946._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578946._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578946._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578946._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327745.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578949._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327761.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578950._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327764.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578951._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578954._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327862.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578955._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327862.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578955._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327862.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578955._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00327862.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578955._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328017.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578956._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328042.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578957._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328042.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578957._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328042.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578957._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328099.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578961._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328099.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578961._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328099.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578961._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328099.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578961._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328099.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578961._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328099.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578961._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328221.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578963._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578964._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328333.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578967._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328333.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578967._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328333.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578967._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328333.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578967._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328374.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578968._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328393.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578970._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328393.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578970._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328393.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578970._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328393.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578970._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00328393.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578970._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329385.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578971._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329484.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578972._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578973._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329716.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578974._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329778.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578975._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329780.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578976._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329780.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578976._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329780.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578976._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329829.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578978._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329835.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578980._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329869.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578981._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329869.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578981._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329869.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578981._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329964.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578982._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329964.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578982._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00329964.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578982._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330025.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578983._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330025.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578983._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330025.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578983._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330025.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578983._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330074.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578985._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330074.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578985._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330079.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578986._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330101.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578988._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330160.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578990._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330166.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578991._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330203.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578992._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330203.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578992._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330203.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578992._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330203.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578992._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330294.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578993._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330294.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578993._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330294.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578993._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330294.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578993._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330328.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578996._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330470.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12578998._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330857.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579002._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330874.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579005._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00330875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579008._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331019.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579010._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331020.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579014._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331033.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579015._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331033.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579015._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331082.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579018._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331085.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579019._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._0000010.tree.roo");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331129.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579023._0000011.tree.roo");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579028._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579028._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579028._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579028._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331239.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579029._0000010.tree.roo");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331462.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579032._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331462.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579032._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331462.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579032._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331462.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579032._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331479.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579036._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331479.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579036._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331479.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579036._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331697.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579037._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331697.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579037._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331697.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579037._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579038._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331742.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579042._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331772.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579043._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331804.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579044._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331825.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579047._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331825.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579047._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331825.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579047._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331860.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579050._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331875.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579052._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331905.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579054._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331951.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579055._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331951.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579055._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331951.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579055._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000011.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00331975.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579059._000012.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332303.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579063._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332304.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579073._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332304.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579073._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332304.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579073._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332304.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579073._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332304.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579073._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332720.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579075._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332896.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579077._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579080._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579080._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579080._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579080._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332953.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579082._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332953.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579082._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332953.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579082._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332953.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579082._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332955.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579086._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332955.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579086._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00332955.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579086._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333181.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579087._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333181.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579087._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333181.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579087._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333181.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579087._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333181.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579087._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333192.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579088._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333192.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579088._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333192.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579088._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333192.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579088._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333192.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579088._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333367.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579092._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333367.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579092._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333367.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579092._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333367.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579092._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333367.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579092._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333380.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579095._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333380.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579095._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333380.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579095._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333426.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579099._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333469.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579100._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333469.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579100._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333469.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579100._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579101._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579101._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579101._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579101._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579101._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579101._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333519.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579104._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333519.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579104._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333519.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579104._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333519.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579104._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333650.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579105._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333650.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579105._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333650.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579105._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333650.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579105._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333650.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579105._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333707.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579109._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333707.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579109._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333707.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579109._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333778.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579111._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333778.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579111._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333778.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579111._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333778.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579111._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333778.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579111._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333828.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579112._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333828.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579112._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333828.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579112._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333828.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579112._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333853.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579115._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333904.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579117._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333904.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579117._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333904.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579117._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333904.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579117._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333904.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579117._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333979.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579120._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333979.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579120._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333979.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579120._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00333994.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579121._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334264.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579125._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334264.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579125._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334264.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579125._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334264.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579125._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334317.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579126._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334317.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579126._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334317.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579126._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334350.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579127._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334350.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579127._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334350.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579127._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334350.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579127._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334350.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579127._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334384.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579131._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334413.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579132._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334413.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579132._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334413.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579132._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334413.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579132._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334413.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579132._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334413.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579132._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334443.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579136._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334443.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579136._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334443.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579136._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334443.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579136._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334443.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579136._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334455.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579137._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334487.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579138._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334564.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579141._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334564.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579141._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334564.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579141._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334564.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579141._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334580.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579143._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334588.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579147._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334588.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579147._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334588.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579147._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334588.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579147._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334637.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579148._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334637.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579148._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334637.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579148._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579149._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579149._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579149._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579152._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579152._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334710.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579152._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334779.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579157._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334779.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579157._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334779.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579157._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334842.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579159._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334842.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579159._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334842.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579159._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334849.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579160._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334849.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579160._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334849.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579160._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334878.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579163._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334878.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579163._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334878.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579163._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334960.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579168._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334960.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579168._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334960.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579168._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334960.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579168._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334960.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579168._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334993.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579172._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334993.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579172._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334993.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579172._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334993.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579172._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334993.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579172._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00334993.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579172._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335022.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579174._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335022.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579174._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335022.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579174._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335022.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579174._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335056.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579176._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335082.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579177._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335083.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579179._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335083.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579179._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335083.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579179._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335083.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579179._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335083.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579179._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335083.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579179._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335131.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579180._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335170.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579181._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335170.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579181._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335170.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579181._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335170.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579181._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335170.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579181._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335170.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579181._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335177.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579182._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335282.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579184._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335282.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579184._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335282.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579184._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335290.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579185._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335290.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579185._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335290.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579185._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335290.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579185._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00335302.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579186._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336497.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579187._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336505.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579188._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336506.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579190._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336506.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579190._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336506.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579190._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336548.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579191._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336548.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579191._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336548.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579191._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336548.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579191._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336548.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579191._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336567.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579192._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336630.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579193._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336678.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579194._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336719.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579196._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336719.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579196._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336719.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579196._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336719.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579196._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336719.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579196._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336782.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579197._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336832.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579198._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000014.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000015.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000016.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000011.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000012.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000013.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336852.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579199._000017.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579202._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579202._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579202._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579202._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336915.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579202._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336927.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579204._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336944.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579205._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336944.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579205._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336944.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579205._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336944.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579205._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336998.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579207._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336998.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579207._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336998.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579207._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00336998.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579207._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337005.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579208._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337005.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579208._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337005.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579208._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337052.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579209._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337107.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579210._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337156.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579211._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337156.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579211._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337156.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579211._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337176.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579212._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337215.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579213._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337263.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579214._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337335.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579215._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337335.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579215._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337335.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579215._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337335.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579215._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337335.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579215._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337335.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579215._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337371.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579217._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337371.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579217._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337371.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579217._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337371.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579217._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337371.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579217._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337404.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579218._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337404.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579218._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337404.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579218._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337404.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579218._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337404.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579218._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337451.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579219._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337491.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579220._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337542.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579222._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000012.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000013.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000011.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337662.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579225._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00337705.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579226._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338183.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579227._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338220.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579228._000011.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000012.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000013.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000011.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338259.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579230._000010.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000001.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000002.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000003.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000004.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000005.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000006.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000007.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000008.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000009.tree.root");
  Files.push_back(pathFiles + "user.ecorriga.data17_13TeV.00338349.physics_Main.trijet.data17-15-nov_tree.root/user.ecorriga.12579231._000010.tree.root");
  
  
      
      
	
  }else if (m_MC && m_isjjj  && m_signal){ //EOS   //same for data 17/16
//     Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.data.03.08.2017_tree.root/*.root"); //CHANGE
 
    if (m_onGrid==false){ pathFiles="/eos/atlas/user/m/mdevesa/Signal/";}

    std::cout << "Sample Signal jjj" << std::endl;

    if(m_mass=="250GeV"){
      //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305534.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412539._000001.tree.root");  
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305535.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412540._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305536.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412541._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305537.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp25_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412542._000001.tree.root");
    }
    if(m_mass=="350GeV"){
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305538.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412543._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305539.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412544._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305540.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412545._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305541.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp35_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412546._000001.tree.root");
    } 
    if(m_mass=="450GeV"){   
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305542.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412547._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305543.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412548._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305544.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412549._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305545.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp45_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412550._000001.tree.root");
    }
    if(m_mass=="550GeV"){    
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305546.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp1_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412551._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305547.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp2_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412552._000001.tree.root");
     //Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305548.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp3_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412553._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305549.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350_mRp55_mD10_gSp4_gD1.trijet.20171023-01_tree.root/user.kkrizka.12412554._000001.tree.root");
    } 
    
  }else if (m_MC && !m_signal && m_isjjj && m_Sherpa){ ///EOS
  
    if (m_onGrid==false){ pathFiles="/eos/atlas/user/m/mdevesa/MCsample/";}
  
    std::cout << "Sample MC jjj" << std::endl;
    
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.trijet.20171023-01_tree.root/user.kkrizka.12412522._000001.tree.root");      
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.trijet.20171023-01_tree.root/user.kkrizka.12412522._000002.tree.root");      
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426131.Sherpa_CT10_jets_JZ1.trijet.20171023-01_tree.root/user.kkrizka.12412522._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.trijet.20171023-01_tree.root/user.kkrizka.12412524._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.trijet.20171023-01_tree.root/user.kkrizka.12412524._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426132.Sherpa_CT10_jets_JZ2.trijet.20171023-01_tree.root/user.kkrizka.12412524._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426133.Sherpa_CT10_jets_JZ3.trijet.20171023-01_tree.root/user.kkrizka.12412526._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426133.Sherpa_CT10_jets_JZ3.trijet.20171023-01_tree.root/user.kkrizka.12412526._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.trijet.20171023-01_tree.root/user.kkrizka.12412528._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.trijet.20171023-01_tree.root/user.kkrizka.12412528._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.trijet.20171023-01_tree.root/user.kkrizka.12412528._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.trijet.20171023-01_tree.root/user.kkrizka.12412528._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426134.Sherpa_CT10_jets_JZ4.trijet.20171023-01_tree.root/user.kkrizka.12412528._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.trijet.20171023-01_tree.root/user.kkrizka.12412530._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.trijet.20171023-01_tree.root/user.kkrizka.12412530._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.trijet.20171023-01_tree.root/user.kkrizka.12412530._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.trijet.20171023-01_tree.root/user.kkrizka.12412530._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426135.Sherpa_CT10_jets_JZ5.trijet.20171023-01_tree.root/user.kkrizka.12412530._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426136.Sherpa_CT10_jets_JZ6.trijet.20171023-01_tree.root/user.kkrizka.12412532._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426136.Sherpa_CT10_jets_JZ6.trijet.20171023-01_tree.root/user.kkrizka.12412532._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426137.Sherpa_CT10_jets_JZ7.trijet.20171023-01_tree.root/user.kkrizka.12412533._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426137.Sherpa_CT10_jets_JZ7.trijet.20171023-01_tree.root/user.kkrizka.12412533._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426138.Sherpa_CT10_jets_JZ8.trijet.20171023-01_tree.root/user.kkrizka.12412534._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426138.Sherpa_CT10_jets_JZ8.trijet.20171023-01_tree.root/user.kkrizka.12412534._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426139.Sherpa_CT10_jets_JZ9.trijet.20171023-01_tree.root/user.kkrizka.12412535._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426139.Sherpa_CT10_jets_JZ9.trijet.20171023-01_tree.root/user.kkrizka.12412535._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426140.Sherpa_CT10_jets_JZ10.trijet.20171023-01_tree.root/user.kkrizka.12412536._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426140.Sherpa_CT10_jets_JZ10.trijet.20171023-01_tree.root/user.kkrizka.12412536._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426141.Sherpa_CT10_jets_JZ11.trijet.20171023-01_tree.root/user.kkrizka.12412537._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426141.Sherpa_CT10_jets_JZ11.trijet.20171023-01_tree.root/user.kkrizka.12412537._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.426142.Sherpa_CT10_jets_JZ12.trijet.20171023-01_tree.root/user.kkrizka.12412538._000001.tree.root");

  }else if (m_MC && !m_signal && m_isjjj && !m_Sherpa){ ///EOS

    if (m_onGrid==false){ pathFiles="/1/mdaneri/Pythia_Trijet/";}

    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.trijet.20171131-01_tree.root/user.kkrizka.12812143._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.trijet.20171131-01_tree.root/user.kkrizka.12812143._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.trijet.20171131-01_tree.root/user.kkrizka.12812146._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.trijet.20171131-01_tree.root/user.kkrizka.12812146._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.trijet.20171131-01_tree.root/user.kkrizka.12812146._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.trijet.20171131-01_tree.root/user.kkrizka.12812146._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000009.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000010.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W.trijet.20171131-01_tree.root/user.kkrizka.12812149._000011.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W.trijet.20171131-01_tree.root/user.kkrizka.12812151._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000005.tree.root.3");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.trijet.20171131-01_tree.root/user.kkrizka.12812154._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000009.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000010.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000011.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000012.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000013.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000014.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000015.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W.trijet.20171131-01_tree.root/user.kkrizka.12812157._000016.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000004.tree.root.2");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W.trijet.20171131-01_tree.root/user.kkrizka.12812160._000009.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000009.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000010.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000011.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000012.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000013.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000014.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W.trijet.20171131-01_tree.root/user.kkrizka.12812163._000015.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.trijet.20171131-01_tree.root/user.kkrizka.12812167._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.trijet.20171131-01_tree.root/user.kkrizka.12812167._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W.trijet.20171131-01_tree.root/user.kkrizka.12812167._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000009.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000010.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000011.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W.trijet.20171131-01_tree.root/user.kkrizka.12812171._000012.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W.trijet.20171131-01_tree.root/user.kkrizka.12812173._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W.trijet.20171131-01_tree.root/user.kkrizka.12812176._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000008.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000009.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000010.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000011.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W.trijet.20171131-01_tree.root/user.kkrizka.12812180._000012.tree.root");
  
  }else if (m_MC && m_signal && !m_isjjj){ //EOS  // gjj
//     Files.push_back(pathFiles+"user.kpachal.data16_13TeV.periodB.physics_Main.trijet.data.03.08.2017_tree.root/*.root"); //CHANGE
  
    if (m_onGrid==false){ pathFiles="/1/mdaneri/Signal_dijetgamma/";}

    std::cout << "Sample signal gjj" << std::endl;

    if(m_mass=="300GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305161.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp3_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412632._000001.tree.root");}
    if(m_mass=="400GeV"){ 
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305162.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp4_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412640._000001.tree.root");}
    if(m_mass=="500GeV"){    
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305163.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp5_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412642._000001.tree.root");}
    if(m_mass=="250GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305465.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp25_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412643._000001.tree.root");}
    if(m_mass=="350GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305468.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp35_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412647._000001.tree.root");}
    if(m_mass=="450GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305471.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp45_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412652._000001.tree.root");}
    if(m_mass=="550GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305474.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp55_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412656._000001.tree.root");}
    if(m_mass=="750GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305478.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp75_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412661._000001.tree.root");}
    if(m_mass=="950GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305481.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp95_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412664._000002.tree.root");}
    if(m_mass=="1500GeV"){
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305483.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mR1p5_mD10_gSp3_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412667._000001.tree.root");}
    else{ 
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305466.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp25_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412644._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305467.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp25_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412646._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305469.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp35_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412648._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305470.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp35_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412651._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305472.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp45_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412653._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305473.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp45_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412655._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305475.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp55_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412657._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305476.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp55_mD10_gSp1_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412659._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305477.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp75_mD10_gSp4_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412660._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305479.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp75_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412663._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.305482.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mR1p5_mD10_gSp4_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412665._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308967.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp25_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412668._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308968.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp35_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412669._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308969.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp45_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412671._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308970.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp55_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412672._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308971.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp75_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412673._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308972.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mRp95_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412675._000001.tree.root");
     Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.308973.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph50_mR1p5_mD10_gSp2_gD1.dijetgamma.20171023-01_tree.root/user.kkrizka.12412676._000001.tree.root");
    }

  }else if (m_MC && !m_signal && !m_isjjj){ ///EOS   //Gjj
  
    if (m_onGrid==false){ pathFiles="/1/mdaneri/Sherpa_dijetgamma/";}
  
    std::cout << "Sample mc gjj" << std::endl;
    
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000001.tree.root");      
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000003.tree.root");      
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000005.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000007.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412573._000006.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412575._000001.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412575._000002.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412575._000003.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412575._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412576._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412576._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412576._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412577._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412577._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412577._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412579._000001.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412579._000002.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412579._000003.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412580._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412580._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412582._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412582._000002.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412582._000003.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412582._000004.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412583._000001.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412583._000002.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412583._000003.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412584._000001.tree.root"       );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412584._000002.tree.root"       );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412586._000001.tree.root"    );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412587._000001.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412587._000002.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412587._000003.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412589._000001.tree.root"       );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412589._000002.tree.root"       );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412590._000001.tree.root"   );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412590._000002.tree.root"   );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412592._000001.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412592._000002.tree.root" );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412593._000001.tree.root"      );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412593._000002.tree.root"      );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412594._000001.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412594._000002.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412596._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412596._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412597._000001.tree.root"     );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412597._000002.tree.root"     );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412598._000001.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412598._000002.tree.root"  );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412600._000001.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412600._000002.tree.root");
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361059.Sherpa_CT10_SinglePhotonPt2000_4000_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412601._000001.tree.root"     );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361060.Sherpa_CT10_SinglePhotonPt4000_CVetoBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412602._000001.tree.root"       );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361061.Sherpa_CT10_SinglePhotonPt4000_CFilterBVeto.dijetgamma.20171023-01_tree.root/user.kkrizka.12412604._000001.tree.root"     );
    Files.push_back(pathFiles+"user.kkrizka.mc16_13TeV.361062.Sherpa_CT10_SinglePhotonPt4000_BFilter.dijetgamma.20171023-01_tree.root/user.kkrizka.12412605._000001.tree.root"          );
  }
  
 
   std::cout << "Files  : " << Files.size() << std::endl;

    return;


}// end GetListOfRootFiles

