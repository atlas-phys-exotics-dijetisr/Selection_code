#include <iostream>
#include <vector>
#include <iomanip>

#include "TFile.h"
#include "TObject.h"
#include "TKey.h"
#include "TF1.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TLatex.h"
#include "TStyle.h"
#include "TLine.h"
#include "TLegend.h"
#include <TROOT.h>

#include "TSystem.h"
#include "TGaxis.h"

#include <cstdlib>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TObjString.h"


#if not defined(__CINT__) || defined(__MAKECINT__)
// needs to be included when makecint runs (ACLIC)

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#endif


void Enable_and_Set_Branches(TTree* & tree);
bool plot_discriminants(int min, int max);
bool TMVA_(int min, int max);
bool Plot_this (TH1F* h_y_data_,TH1F* h_y_signal_,TH1F* h_S_vs_Bkg_,int min_,int max_,std::string hist_name_,double xMaximus_,double Maximus_);
std::string trigger_type;

bool debug,gjj,single,trigger16,tmva;

double_t Weight; double_t Mass;


  
int main(int argc, char* argv[]) {

  //---------------------------
  // Decoding the user settings
  //---------------------------
  for (int i=1; i< argc; i++){

    std::string opt(argv[i]); std::vector< std::string > v;

    std::istringstream iss(opt);

    std::string item;
    char delim = '=';

    while (std::getline(iss, item, delim)){
        v.push_back(item);
    }
    // Only for 2016data 
  
    if ( opt.find("--gjj=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) gjj= true;
      if (v[1].find("FALSE") != std::string::npos) gjj= false;
    }
    if ( opt.find("--debug=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) debug= true;
      if (v[1].find("FALSE") != std::string::npos) debug= false;
    }
    if ( opt.find("--Tsingle=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) single = true;
      if (v[1].find("FALSE") != std::string::npos) single = false;
    }
    if ( opt.find("--trigger16=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) trigger16= true;
      if (v[1].find("FALSE") != std::string::npos) trigger16= false;
    }
    if ( opt.find("--tmva=")      != std::string::npos) {
      if (v[1].find("TRUE") != std::string::npos) tmva= true;
      if (v[1].find("FALSE") != std::string::npos) tmva= false;
    }
  
  }//End: Loop over input options

  
  //Set the inicial mass range and the numer of ranges
  unsigned int N=1;int Mmin;
  if(!gjj){N=5;Mmin=200;}else{N=5;Mmin=200;} //this is different for jjj and gjj samples
 
  //Loop over ranges
  for (unsigned int k=1; k<2; ++k){
    //Define min and max of each range
    int min = Mmin + (k-1)* 100;
    int max = Mmin + k * 100;
    
    plot_discriminants( min, max);
    
    if (tmva) { TMVA_(min, max);}
    }  
}

bool TMVA_(int min, int max)
{     
/* 
    
    gErrorIgnoreLevel = 3000;
    gROOT->Reset();
//     gROOT->ProcessLine(".L loader.C+");
         
    gStyle->SetOptStat(0);
         
    ///////////////
    //    MVTA   // 
    ///////////////
         
   //Load MLP and TMVA libraries
    gROOT->SetStyle("Plain");
    gSystem->Load("libMLP");
    gSystem->Load("libTMVA.so");
    
    //Set up the output file 
    TString folder =  TString::Format("./%iM%i/",min,max ) ;
    TFile f(folder + "TMVA.root","RECREATE");
    TMVA::Factory* factory = new TMVA::Factory( "TMVAanalysis", &f, "" );
        
    std::string sample_type = "";
    if (gjj) {sample_type = "gamma_dijet";}
    else{sample_type = "trijet";}
    
    trigger_type = "";
    if (trigger16 && !gjj) {trigger_type = "HLT_j380";}
    else if(!single && gjj){trigger_type = "HLT_g75_tight_3j50noL1_L1EM22VHI";}
    else if (gjj && single){trigger_type = "HLT_g140_loose";}
      
    // Get input file
    TFile * f_mc = TFile::Open(TString::Format("./optimization_outputs/cuts_MC_%iGeV_%s_%s.root",min + 50,sample_type.c_str(),trigger_type.c_str()));
    if (!f_mc->IsOpen()) {std::cerr << "ERROR: cannot open MC.root" << std::endl;}
     
    //Signal
    TFile * f_signal = TFile::Open(TString::Format("./optimization_outputs/cuts_Signal_%iGeV_%s_%s.root",min + 50,sample_type.c_str(),trigger_type.c_str()));
    if (!f_signal->IsOpen()) {std::cerr << "ERROR: cannot open Signal.root" << std::endl;}

    TTree * tsignal = (TTree*) f_signal->Get("Tree_");
    TTree * tbg     = (TTree*) f_mc->Get("Tree_");

    ///////////////////
    //  Cross check  //
    ///////////////////
    
    double SIGNAL=0;double BKG=0;double SIGNAL_nocuts=0;double BKG_nocuts=0;
    ///////////////////////////////////////////////////////////
    TTree * tsignal_nocuts = (TTree*) f_signal->Get("Tree_noCuts");
    Enable_and_Set_Branches(tsignal_nocuts);
    
    for(Long64_t i_event=0; i_event < tsignal_nocuts->GetEntries() ; i_event++){
      //Get the information from the tree
      tsignal_nocuts->GetEntry(i_event);
      SIGNAL_nocuts += Weight;
    }
    std::cerr << "N SIGNAL no cuts:---->"<< SIGNAL_nocuts << std::endl;
    
    Enable_and_Set_Branches(tsignal);
    for(Long64_t i_event=0; i_event < tsignal->GetEntries() ; i_event++){
      //Get the information from the tree
      tsignal->GetEntry(i_event);
      SIGNAL += Weight;
    }
    std::cerr << "N SIGNAL:---->"<< SIGNAL << std::endl;

    //--------------------------------------------------------------------------------------
    TTree * tbg_nocuts     = (TTree*) f_mc->Get("Tree_noCuts");
    Enable_and_Set_Branches(tbg_nocuts);
    for(Long64_t i_event=0; i_event < tbg_nocuts->GetEntries() ; i_event++){
      //Get the information from the tree
      tbg_nocuts->GetEntry(i_event);
      BKG_nocuts += Weight;
     
    }
    std::cerr << "N BKG no cuts:---->"<< BKG_nocuts << std::endl;

    Enable_and_Set_Branches(tbg);
    for(Long64_t i_event=0; i_event < tbg->GetEntries() ; i_event++){
      //Get the information from the tree
      tbg->GetEntry(i_event);
      BKG+= Weight;
      
    }
    std::cerr << "N BKG:---->"<< BKG << std::endl;

 
    //Set up the input data trees to use with TMVA
    factory->SetWeightExpression( "Weight" );	
    factory->SetInputTrees(tsignal,tbg);
    //Set N for training
//     by default it split half and half. but if you prefere to set the numbers you can use the folwing command
//     factory->PrepareTrainingAndTestTree("",tsignal->GetEntries() /2, tbg->GetEntries() /2, tsignal->GetEntries() /2, tbg->GetEntries() /2);
    
    //define the variables to use in MVA
    std::vector<TString>* discriminant_name = new std::vector<TString>;
      discriminant_name->push_back ("Y");
     discriminant_name->push_back ("Asy");//DR_closest_jet

    factory->SetInputVariables(discriminant_name);
    //Book the MVA method
    if (debug) std::cout << "Book the MVA method: "<< std::endl;
    
 //    factory->BookMethod(TMVA::Types::kLikelihood, "Likelihood");
      factory->BookMethod(TMVA::Types::kCuts,"Cuts");
//     factory->BookMethod(TMVA::Types::kFisher,"Fisher");
   
   
    //train MVAs
    if (debug) std::cout << "train MVAs: "<< std::endl;
    factory->TrainAllMethods();
    if (debug) std::cout << "test all methods : "<< std::endl;
    factory->TestAllMethods();
    //Evaluate variables
    if (debug) std::cout << "Evaluate variables: "<< std::endl;
    factory->EvaluateAllMethods();
    if (debug) std::cout << "Close: "<< std::endl;
    //Salve the output
    f.Close();
 

    f_mc -> Close();f_signal->Close();
 */
}



bool plot_discriminants(int min, int max)
{     

    
    gErrorIgnoreLevel = 3000;
    gROOT->Reset();
//     gROOT->ProcessLine(".L loader.C+");
         
    gStyle->SetOptStat(0);
       
    std::string sample_type = "";
    if (gjj) {sample_type = "gamma_dijet";}
    else{sample_type = "trijet";}
    
    trigger_type = "";
    if (trigger16 && !gjj) {trigger_type = "HLT_j380";}
    else if(!single && gjj){trigger_type = "HLT_g75_tight_3j50noL1_L1EM22VHI";}
    else if (gjj && single){trigger_type = "HLT_g140_loose";}
      
    // Get input file
    TFile * f_mc = TFile::Open(TString::Format("./optimization_outputs/cuts_MC_%iGeV_%s_%s.root",min + 50,sample_type.c_str(),trigger_type.c_str()));
    if (!f_mc->IsOpen()) {std::cerr << "ERROR: cannot open MC.root" << std::endl;}
     
    //Signal
    TFile * f_signal = TFile::Open(TString::Format("./optimization_outputs/cuts_Signal_%iGeV_%s_%s.root",min + 50,sample_type.c_str(),trigger_type.c_str()));
    if (!f_signal->IsOpen()) {std::cerr << "ERROR: cannot open Signal.root" << std::endl;}

    TTree * tsignal = (TTree*) f_signal->Get("Tree_");
    TTree * tbg     = (TTree*) f_mc->Get("Tree_");

    ///////////////////
    //  Cross check  //
    ///////////////////
    
    double SIGNAL=0;double BKG=0;double SIGNAL_nocuts=0;double BKG_nocuts=0;
  
    Enable_and_Set_Branches(tsignal);
    for(Long64_t i_event=0/*abs(2*(tsignal->GetEntries()/3))*/; i_event < tsignal->GetEntries() ; i_event++){
      //Get the information from the tree
      tsignal->GetEntry(i_event);
      SIGNAL += Weight;
    }
    std::cerr << "N SIGNAL:---->"<< SIGNAL << std::endl;

    //--------------------------------------------------------------------------------------

    Enable_and_Set_Branches(tbg);
    for(Long64_t i_event=0/*abs(2*(tbg->GetEntries()/3))*/; i_event < tbg->GetEntries() ; i_event++){
      //Get the information from the tree
      tbg->GetEntry(i_event);
      BKG+= Weight;
    }
    std::cerr << "N BKG:---->"<< BKG << std::endl;

 


    ////////////////////////
    // Plot Distributions //
    ////////////////////////
    
    //List of variables for each mass range    
    std::vector<std::string>* hist_name     = new std::vector<std::string>;
    hist_name->push_back ("y*"   );
    hist_name->push_back ("Asy"  );
    hist_name->push_back ("DeltaR"     );
    hist_name->push_back ("DeltaR_wrt_Z" ); 
    hist_name->push_back ("Asy_ISR_Zprime"  );
    hist_name->push_back ("Pt_diff_ISR_jet1");
    hist_name->push_back ("Pt_diff_ISR_jet2");
    hist_name->push_back ("DPhi_ISR_jet1"   );
    hist_name->push_back ("DPhi_ISR_jet2"   );
    hist_name->push_back ("DPhi_ISR_Zprime" );
    hist_name->push_back ("Thrust"          );

    
    // Loop over variables
    for(unsigned int i =0 ;i < hist_name->size() ;++i ){ 
      
      //Define name of the distribution
      TString name = TString::Format("%s",hist_name->at(i).c_str()) ;
      //Get both distributions 
      TH1F * h_MC     = (TH1F *) f_mc     -> Get(name);
      TH1F * h_Signal = (TH1F *) f_signal -> Get(name);

      if (h_MC == 0 ) {  std::cout << "ALERT: empty histograms for MC: "<<name<< std::endl;}
      if (h_Signal == 0 ) {  std::cout << "ALERT: empty histograms for Signal: "<<name<< std::endl;}
           

      ///////////////
      // s/sqrt(B) //
      ///////////////
      
      // Get the s/sqrt(B) distribution
      TH1F* h_S_vs_Bkg = (TH1F*) h_Signal-> Clone ("s/sqrt(B)");
      
      
      //Normalized
      double Norm_MC = h_MC    ->Integral() ;
      double Norm_Signal = h_Signal->Integral() ;
      
	
      //Get (xmaximus ; maximus) of the s/sqrt(B) distribution
      double Maximus=0;float xMaximus=0;
      for ( int bin = 0 ; bin < h_Signal->GetNbinsX(); ++bin){
	
//     	if (h_Signal ->GetBinCenter(bin)< 0.79 && h_MC ->GetBinCenter(bin)< 0.79 ){
	//For each bin, compute the integral from cero to the bin
	double I_signal = h_Signal ->Integral(0,bin)*(SIGNAL)/Norm_Signal ;
	double I_data   = h_MC     ->Integral(0,bin)*(BKG)/Norm_MC;
	double F;
	
	//Define de bin content 
	if (I_data!=0){F= I_signal /sqrt(I_data);}else{F=0;}
	
	h_S_vs_Bkg -> SetBinContent (bin, F);
	h_S_vs_Bkg -> SetBinError   (bin, 0.0001);
	
	//Compute de maximus
	if (Maximus < F){Maximus = F ; xMaximus = h_S_vs_Bkg->GetXaxis()->GetBinCenter(bin);}
// 	}else	{
// 	  h_S_vs_Bkg -> SetBinContent (bin, 0);
// 	  h_S_vs_Bkg -> SetBinError   (bin, 0.0001);
// 	}
      }//end loop over bins
      
      //To Normalize
      h_Signal-> Scale(1/Norm_Signal);
      h_MC    -> Scale(1/Norm_MC);
      h_S_vs_Bkg->Scale(1/h_S_vs_Bkg->Integral());
      
      //////////
      // Plot //
      //////////
      
      //Plot each distribution comparison
      Plot_this (h_MC, h_Signal, h_S_vs_Bkg, min, max, hist_name->at(i), xMaximus, Maximus );
	
    } //end loop over variables

    f_mc -> Close();f_signal->Close();
 
}


bool Plot_this (TH1F* h_MC_, TH1F* h_Signal_, TH1F* h_S_vs_Bkg_ ,int min_ ,int max_ ,std::string hist_name_ ,double xMaximus_, double Maximus_ ){
  
  //Define de canvas , lagend , and Frame
  TCanvas*  c   = new TCanvas("c","c",600,400);
  TLegend* leg  = new TLegend(0.7,0.7,0.9,0.85);
      
  gStyle->SetOptStat(0);
  gStyle->SetTitleXOffset(1.2);
  gStyle->SetTitleYOffset(1.2);
  
  // Define bining
  float nbins=1;float nbinsy=10;
  float min=0  ;float miny=0;
  float max=1  ;float maxy=1;
  // List of discriminants 
  if (hist_name_== "y*")                                         {nbins= 30 ;min=0     ;max= 2.9  ; nbinsy=70 ;miny=0.001;maxy=0.07 ;} 
  if (hist_name_== "Asy" )                                       {nbins= 99 ;min=0     ;max= 0.99 ; nbinsy=24 ;miny=0.001;maxy=0.025;}
  if (hist_name_== "DeltaR" || hist_name_=="DeltaR_wrt_Z" )      {nbins= 100;min=0.5   ;max= 4.9  ; nbinsy=65 ;miny=0.   ;maxy=0.065;}
  if (hist_name_== "Asy_ISR_Zprime"   )                          {nbins= 200,min= -1   ,max= 1    , nbinsy=70 ;miny=0.   ;maxy=0.07 ;}
  if (hist_name_== "Pt_diff_ISR_jet1" )                          {nbins= 300,min=  1   ,max= 4.9  , nbinsy=40 ;miny=0.   ;maxy=0.04 ;} 
  if (hist_name_== "Pt_diff_ISR_jet2" )                          {nbins= 500,min=  1   ,max= 25   , nbinsy=40 ;miny=0.   ;maxy=0.04 ;}
  if (hist_name_== "DPhi_ISR_jet1"    )                          {nbins= 400,min=  -3.5,max= 3.5  , nbinsy=40 ;miny=0.   ;maxy=0.04 ;}
  if (hist_name_== "DPhi_ISR_jet2"    )                          {nbins= 400,min=  -3.5,max= 3.5  , nbinsy=40 ;miny=0.   ;maxy=0.04 ;}
  if (hist_name_== "DPhi_ISR_Zprime"  )                          {nbins= 400,min=  -3.5,max= 3.5  , nbinsy=30 ;miny=0.   ;maxy=0.03 ;}
  if (hist_name_== "Thrust" )                                    {nbins= 150,min=  0.7 ,max= 1.19 , nbinsy=100;miny=0.   ;maxy=0.1  ;}
 
    
  //Define frame with the correct binning    
  TH2F frame("frame","",nbins,min,max,nbinsy,miny,maxy);
  
  frame.SetTitleSize(0.04,"x");
  frame.SetTitleSize(0.04,"y");  
  frame.GetXaxis()->SetTitle(hist_name_.c_str()); 
  frame.GetYaxis()->SetTitle("1/Events");
  
  if (hist_name_== "y*")                { frame.GetXaxis()->SetTitle("y^{*}_{jj}");} 
  if (hist_name_== "Asy" )              { frame.GetXaxis()->SetTitle("(|p_{T}^{1}|-|p_{T}^{2}|)/(|p_{T}^{1}|+|p_{T}^{2}|)");}
  if (hist_name_== "DeltaR" )           { frame.GetXaxis()->SetTitle("#DeltaR_{ISR,close-j}");}
  if (hist_name_== "DeltaR_wrt_Z" )     { frame.GetXaxis()->SetTitle("#DeltaR_{ISR,jj}");}
  if (hist_name_== "Thrust" )           { frame.GetXaxis()->SetTitle("Thrust") ;}                                   
  if (hist_name_== "Asy_ISR_Zprime"   ) { frame.GetXaxis()->SetTitle("(|p_{T}^{ISR}|-|p_{T}^{Reso}|)/(|p_{T}^{ISR}|+|p_{T}^{Reco}|)");}
  if (hist_name_== "Pt_diff_ISR_jet1" ) { frame.GetXaxis()->SetTitle("pt difference ISR/jet1" );}
  if (hist_name_== "Pt_diff_ISR_jet2" ) { frame.GetXaxis()->SetTitle("pt difference ISR/jet2" );}
  if (hist_name_== "DPhi_ISR_jet1"    ) { frame.GetXaxis()->SetTitle("#DeltaPhi{ISR,jet1}");}
  if (hist_name_== "DPhi_ISR_jet2"    ) { frame.GetXaxis()->SetTitle("#DeltaPhi{ISR,jet2}" );}
  if (hist_name_== "DPhi_ISR_Zprime"  ) { frame.GetXaxis()->SetTitle("#DeltaPhi{ISR,Zprime}" );}
         
  
  //Define colors and stuff for the plot
  h_Signal_  -> SetMarkerColor(kBlue);      h_Signal_  ->SetMarkerStyle(26);  h_Signal_   ->SetMarkerSize(0.8);
  h_MC_      -> SetMarkerColor(kBlack);     h_MC_      ->SetMarkerStyle(23);  h_MC_       ->SetMarkerSize(0.8);
  h_S_vs_Bkg_-> SetLineColor(kRed );       h_S_vs_Bkg_ ->SetMarkerSize(0.6);
 
  //Draw distributions
  frame.Draw("AXIS");
  h_Signal_  -> Draw("SAME");
  h_MC_      -> Draw("SAME");
  h_S_vs_Bkg_-> Draw("HIST L same");

  float lx;float ly;
  if (hist_name_== "y*" || hist_name_== "DeltaR"|| hist_name_== "Thrust") { lx=0.7;ly= 0.35; }
  else {lx=0.35;ly= 0.65;}
  
  TLegend legend(lx,ly,lx+0.2,ly+0.2);
  legend.SetBorderSize(0);  // no border  
  legend.SetTextSize(0.04);
  legend.SetTextFont(42);
  legend.SetFillColor(kWhite);
  legend.AddEntry(h_MC_      , "bkg"       , "p");
  legend.AddEntry(h_Signal_  , "sig"       , "p");
  legend.AddEntry(h_S_vs_Bkg_, "S/#sqrt{B}", "L");
  legend.Draw();
  
  Double_t X=0.6;
  Double_t Y=0.85;
  
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(kBlack);
  l.DrawLatex(X,Y,"ATLAS");
  
  TLatex p;
  p.SetNDC();
  p.SetTextFont(42);
  p.SetTextSize(0.04);
  p.SetTextColor(kBlack);
  p.DrawLatex(X+0.1,Y,"Internal");
  p.DrawLatex(X,Y-0.07,"Simulation, #sqrt{s}=13TeV");
  p.DrawLatex(X,Y-0.12,  TString::Format("%iGeV < M_{jj} < %iGeV" ,min_,max_ ));
  p.DrawLatex(X,Y-0.18, TString::Format("Optimal cut: %.2f"       ,xMaximus_ ));
  p.DrawLatex(X,Y-0.23, TString::Format("Max s/#sqrt{b}: %.4f"    ,Maximus_  ));
  // p.DrawLatex(X,Y-0.18, TString::Format("Trigger: %s",trigger_type.c_str()));

  //Save
  c->SaveAs( "./discriminants_plots/"+hist_name_ + TString::Format("_%iGeVMjj%i_",min_,max_ )+trigger_type+".png"); 

  delete c;delete leg;

  return true;
}

void Enable_and_Set_Branches(TTree* & tree){

  tree->SetBranchStatus("*",0); //disable all branches

  tree->SetBranchStatus  ("Mass",  1);
  tree->SetBranchAddress ("Mass",  &Mass);

  tree->SetBranchStatus  ("Weight",  1);
  tree->SetBranchAddress ("Weight",  &Weight);
}
