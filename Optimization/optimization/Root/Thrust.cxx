
#include <cmath>
#include <algorithm>
#include <numeric>
#include <iostream>
#include "../optimization/Thrust.h"
#include "TVector3.h"

using namespace std;

Thrust::Thrust()
  : _thrust(0)
  , _debug(false)
{
}

Thrust::Thrust( const vector<TVector3>& plist )
  : _thrust(0)
  , _debug(false)
  , _pSum(0,0,0)
  , _plist(plist)
{
  sort(_plist.begin(),_plist.end(),*this);
  _pSum = accumulate( _plist.begin() , _plist.end() , TVector3(0,0,0) );
}

float
Thrust::thrust( const TVector3& thrustAxis ) const
{
  float thrust(0);
  float norm(0);
  TVector3 thrustUnit( thrustAxis.Unit() );
  for( const_iterator i=_plist.begin(), f=_plist.end(); i!=f; ++i) { 
    thrust += abs( thrustUnit.Dot(*i) );
    norm += (*i).Mag();
  }
  return( (norm>0.) ? thrust/norm : -999. );
}


TVector3
Thrust::thrustAxis()
{
  // find a list of starting vectors
  // want four highest momentum objects.

  if(_debug ) { 
    // print them
    cout << "---------------------" << endl;
    for( const_iterator i = _plist.begin(); i != _plist.end(); ++i) { 
      cout << "P: " << (*i).Mag() << endl;
    } 
  }
  
  // sum up all possible combinations of p_i * (+/-1).
  // there are 16 when using the 4 highest momenta, 
  // but thrust is invariant under a flip so
  // need only try 8.

  vector<TVector3> startDirections;
  startDirections.reserve(8);
  unsigned short i;
  unsigned short j;
  for( i=0; i<8; i++ ) {  // wastes time if _plist.size() < 4
    if( _debug )    cout << " --- " << endl;
    TVector3 dir;    
    for( j =  0; j < (_plist.size() >= 4 ? 4: _plist.size()); j++ ) {
      // next line uses bitshift operator to decide whether this guy gets a minus sign multiplier.
      int multiplier = i & (1 << j ) ? 1 : -1;
      if( _debug ) { cout << "Adding " << _plist[j].Mag() << " with multiplier " << multiplier << endl; }
      dir +=  multiplier * _plist[j];
    }
    startDirections.push_back(dir);
  }
  
  // now compute an axis from each of these
  vector<TVector3> guesses;
  guesses.reserve( startDirections.size() );
  for( const_iterator i=startDirections.begin(), f=startDirections.end(); i!=f; ++i ) { 
    guesses.push_back( thrustGuess(*i) );
  }

  // sort them by thrust value and return biggest one
  sort(guesses.begin(),guesses.end(),*this);
  
  for( const_iterator i=guesses.begin(), f=guesses.end(); i!=f; ++i ) { 
    _thrustAxis = *i;
    if( _debug ) {
      cout << "found axis. "
           << _thrustAxis.x() << " "
           << _thrustAxis.y() << " "
           << _thrustAxis.z() << " "
           << thrust( _thrustAxis )
           << endl;
    }
  }  
  _thrustAxis = guesses[0];
  // finally, flip or not to align the vector with the momentum sum
  if( _thrustAxis.Dot(_pSum) < 0 ) {
    TVector3 thrustFlipped(-_thrustAxis.x(),-_thrustAxis.y(),-_thrustAxis.z());
    _thrustAxis = thrustFlipped;
  }
  return _thrustAxis;
}


TVector3
Thrust::thrustGuess( const TVector3& initialDir ) const
{  
  TVector3 newAxis(0,0,0); 
  for( const_iterator i=_plist.begin(), f=_plist.end(); i!=f; ++i ) { 
    newAxis += (*i) * epsilon( initialDir.Unit() * (*i) );
  } 
  double newmag = thrust(newAxis); 
  if( newAxis.Mag() > 1E-10 ) {
    newAxis.SetMag(newmag);
  }
  if( ((newAxis.Unit()).Cross(initialDir.Unit() )).Mag() < 1E-10 ) {
    return newAxis;
  } else {
    return thrustGuess(newAxis);
  }
}
