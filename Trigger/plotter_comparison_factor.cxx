#include <iostream>
#include <vector>
#include <iomanip>

#include "TFile.h"
#include "TObject.h"
#include "TKey.h"
#include "TF1.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TLatex.h"
#include "TStyle.h"
#include "TLine.h"
#include "TLegend.h"
#include <TROOT.h>


std::vector<TString> trigger_prescaled_name;
std::vector<TString> trigger_name;
std::vector<TString> trigger_type;






void all_factor()
{
    gErrorIgnoreLevel = 3000;
    gROOT->Reset();
    gROOT->ProcessLine(".L loader.C+");
    
    gStyle->SetOptStat(0);
    bool isjjj = false;
    bool debug = true;
    // Get input file
    //MC
    TFile * f_mc = TFile::Open("gjj_Signal.root");
    if (!f_mc->IsOpen()) {std::cerr << "ERROR: cannot open MC.root" << std::endl;}
    //Data
    TFile * f_data16 = TFile::Open("gjj_Signal.root");
    if (!f_data16->IsOpen()) {std::cerr << "ERROR: cannot open Data16.root" << std::endl;}
    
    TFile * f_data17 = TFile::Open("gjj_Signal.root");
    if (!f_data17->IsOpen()) {std::cerr << "ERROR: cannot open Data17.root" << std::endl;}
    
    TFile * f_data = TFile::Open("gjj_Signal.root");
    if (!f_data->IsOpen()) {std::cerr << "ERROR: cannot open Data.root" << std::endl;}
    
    //Signal
    TFile * f_signal = TFile::Open("gjj_Signal.root");
    if (!f_signal->IsOpen()) {std::cerr << "ERROR: cannot open Signal.root" << std::endl;}
    
    
    
    TF1 func("fit","[0]+[1]*TMath::Erf((x-[2])/[3])",0,600);
    func.SetParName(0,"Offset");
    func.SetParName(1,"Norma");
    func.SetParName(2,"Turn-On (50%) [GeV]");
    func.SetParName(3,"Rise [GeV]");

    trigger_name.clear(); 
    trigger_prescaled_name.clear();   
    trigger_type.clear();
     
    //Basic Triggers
    if (isjjj){
//     trigger_name.push_back("HLT_j380");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");
//     trigger_name.push_back("HLT_j380");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
//     trigger_name.push_back("HLT_j380");                               trigger_prescaled_name.push_back("HLT_j360");           trigger_type.push_back("single_jet_trigger");
//     trigger_name.push_back("HLT_j380");                               trigger_prescaled_name.push_back("HLT_j260");           trigger_type.push_back("single_jet_trigger");
// 
    trigger_name.push_back("HLT_j400");                               trigger_prescaled_name.push_back("HLT_j260");           trigger_type.push_back("single_jet_trigger");
    trigger_name.push_back("HLT_j400");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
    trigger_name.push_back("HLT_j400");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");

    trigger_name.push_back("HLT_j420");                               trigger_prescaled_name.push_back("HLT_j260");           trigger_type.push_back("single_jet_trigger");
    trigger_name.push_back("HLT_j420");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
    trigger_name.push_back("HLT_j420");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");

    trigger_name.push_back("HLT_j440");                               trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
    trigger_name.push_back("HLT_j440");                               trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("single_jet_trigger");  

    trigger_name.push_back("HLT_3j200");                              trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("multi_jet_trigger");
    trigger_name.push_back("HLT_3j200");                              trigger_prescaled_name.push_back("HLT_j100");           trigger_type.push_back("multi_jet_trigger");
    trigger_name.push_back("HLT_3j200");                              trigger_prescaled_name.push_back("HLT_3j175");          trigger_type.push_back("multi_jet_trigger");

    trigger_name.push_back("HLT_3j175");                              trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("multi_jet_trigger");

    trigger_name.push_back("HLT_j225_gsc400_boffperf_split");         trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
    trigger_name.push_back("HLT_j225_gsc420_boffperf_split");         trigger_prescaled_name.push_back("HLT_j110");           trigger_type.push_back("single_jet_trigger");
    }else{
      trigger_name.push_back("HLT_g140_loose");                         trigger_prescaled_name.push_back("HLT_g120_loose");     trigger_type.push_back("single_jet_trigger");
      trigger_name.push_back("HLT_g120_loose");                         trigger_prescaled_name.push_back("HLT_g50_loose");      trigger_type.push_back("single_jet_trigger");
    }
    
    for(unsigned int k=0 ; k<trigger_name.size(); ++k){

      //Print the trigger name 
      TString name  ="h_pass_"+trigger_name.at(k)+"_and_"+trigger_prescaled_name.at(k)+"__"+trigger_type.at(k);
      if (debug) std::cout << "Opening: " << name <<" with type:  "<<trigger_type.at(k)<< std::endl;

      ////////////
      // Data
      ////////////
      if (debug) std::cout << "Working with Data..." << std::endl;
     
      //built Efficiency 
      if (debug) std::cout << "built Efficiency..." << std::endl;
      TString name_num  = "h_pass_"+trigger_name.at(k)+"_and_"+trigger_prescaled_name.at(k)+"__"+trigger_type.at(k);
      std::string name_num_ = (std::string)name_num;
      TString name_den  = "h_pass_"+trigger_prescaled_name.at(k)+"_for_"+trigger_name.at(k)+"__"+trigger_type.at(k);
      std::string name_den_ = (std::string)name_den;
      TH1F * num = (TH1F *) f_data17->Get(name_num_.c_str());
      TH1F * den = (TH1F *) f_data17->Get(name_den_.c_str());

      if (num == 0 || num->GetEntries()==0) {
	std::cout << "ALERT: empty histograms for efficiency on Data: " << name_num_.c_str() << std::endl;
	continue;
      }
      if (den == 0 || den->GetEntries()==0) {
	std::cout << "ALERT: empty histograms for efficiency on Data: " << name_den_.c_str() << std::endl;
	continue;
      }
      
      // rebin if its necesary
      num->Rebin(2);
      den->Rebin(2);
      // Buit the turn on curve as the ratio between T and t0 , over T0 distribution
      if (debug) std::cout << "compute the turn on curve..." << std::endl;
     
      TGraphAsymmErrors * tg = new TGraphAsymmErrors();
      tg->SetName("h_eff_"+name_num);
      tg->Divide(num, den, "cp");
      tg->GetXaxis()->SetTitle("jet p_{T} (GeV)");
      tg->GetYaxis()->SetTitle("Efficiency");
      
      // Buit the turn on curve copy to change the error and with it make the fit on the full range	
      TGraphAsymmErrors * tge = new TGraphAsymmErrors();
      tge->Divide(num, den, "cp");
      // Override errors to have nice fit
      for(int ii = 0; ii != tge->GetN(); ++ii) {tge->SetPointError(ii, .01,.01,.01,.01);}
      if (debug) std::cout << "Fitting on the full range : " << name << std::endl;

      // Manually 
      double xminfit = 100.;
      double xmaxfit = 600.;

      func.SetParameter(0, 0.5);                     
      func.SetParameter(1, 0.5);                     
      func.SetParameter(2, 200/*in GeV*/);           
      func.SetParameter(3, 50 /*in GeV*/); 
      
      tge->Fit("fit", "", "", xminfit, xmaxfit);
      if (debug) std::cout << "Get some usefull parameters from the fit..." << std::endl;
      double minimum = func.Eval(-1.E6); 
      double plateau = func.Eval(+1.E6); 
      double Xat99   = func.GetX(0.99/**plateau**/);
      double slope   = func.GetParameter(1)/func.GetParameter(3)*(2./TMath::Pi())*1000.;     
      double xminfit_ = func.GetX(0.5);
      double xmaxfit_ = Xat99+40;
      double turnon   = func.GetParameter(2);
      double rise     = func.GetParameter(3);
      
      ///////////////////////////
      // Fit in a reduced range 
      ///////////////////////////
      if (debug)std::cout << "Fit in a reduced range " << std::endl;
      ///New fitting fuction with 3 free parameters
      TF1 func2("fit2","(1+[0])/2+((1-[0])/2)*TMath::Erf((x-[1])/[2])",0,600);
      func2.SetParName(0,"a");
      func2.SetParName(1,"Turn-On (50%) [GeV]");
      func2.SetParName(2,"Rise [GeV]");
      
      func2.SetParameter(0, 0.2);               
      func2.SetParameter(1, turnon /*in GeV*/);           
      func2.SetParameter(2, rise /*in GeV*/);    
      
      //fit in a reduced range
      tg->Fit("fit2"  , "", "", xminfit_  , xmaxfit_  );
  
      tg->GetFunction("fit2")->SetLineColor(kRed);
      tg->GetFunction("fit2")->SetLineWidth(2);
      
      double x99data = func2.GetX(0.99/**plateau**/);
      
      ///////////////
      // Get factors
      ///////////////
      /*TH1F * h_T_only_mc = 0;*/TH1F * h_T_only_signal = 0;TH1F * h_T_only_data = 0;
      for (int kk=0; kk<18/*trigger_name.size()*/ ;++kk){
	 /////// Geting the distributions for the factors
	if (debug) std::cout << "Get T histo ..." << std::endl;
	
	TString name_T_only = TString::Format("h_pass_%s_n_%i_for_all__%s" ,trigger_name.at(k).Data(), kk,trigger_type.at(k).Data());
	std::string name_T_only_ = (std::string)name_T_only;
	//MC
/*	
	h_T_only_mc = (TH1F *) f_mc ->Get(name_T_only_.c_str());
	if (h_T_only_mc==0 ) { std::cout << "ALERT: empty Trigger num distribution (MC): "<<name_T_only_<< std::endl;}*/
	//Signal
	
	if (h_T_only_signal==0 ){  
	h_T_only_signal = (TH1F *) f_signal->Get(name_T_only_.c_str());
	if (h_T_only_signal==0 ) { std::cout << "ALERT: empty Trigger num distribution (signal): "<<name_T_only_<< std::endl;}
	}
	//Data
	if (h_T_only_data==0 ) {
	h_T_only_data = (TH1F *) f_data17->Get(name_T_only_.c_str());
	if (h_T_only_data==0 ) { std::cout << "ALERT: empty Trigger num distribution (data): "<<name_T_only_<< std::endl;} 
	}
     
     
     
     
        if (h_T_only_data==0 || h_T_only_signal==0 /*|| h_T_only_mc==0*/){continue;}else{break;}
        
      }
      double A_signal;double B_signal;double relf_signal;
      double A_data;double B_data;double relf_data;
//       double A_mc;double B_mc;double relf_mc;
      
      
      
      std::cout << "----------data--------- " << std::endl;
      A_data = h_T_only_data->Integral(h_T_only_data->GetBin(x99data),h_T_only_data->GetNbinsX(),"width");
      TH1F * h_data = (TH1F *) f_data17->Get("jet0_pt");
      B_data = h_data->Integral(-1,h_data->GetNbinsX(),"width");
      relf_data =A_data*100/B_data;
      std::cout << "A/B rel-data--> " << relf_data << std::endl;
      
      std::cout << "----------signal--------- " << std::endl;
      A_signal = h_T_only_signal->Integral(h_T_only_signal->GetBin(x99data),h_T_only_signal->GetNbinsX(),"width");
      TH1F * h_signal = (TH1F *) f_signal->Get("jet0_pt");
      B_signal = h_data->Integral(-1,h_signal->GetNbinsX(),"width");
      relf_signal =A_signal*100/B_signal;
      std::cout << "A/B rel-signal--> " << relf_signal << std::endl;
//       std::cout << "----------MC--------- " << std::endl;
// //       A_mc = h_T_only_mc->Integral(h_T_only_mc->GetBin(x99data),h_T_only_mc->GetNbinsX(),"width");
// //       TH1F * h_mc = (TH1F *) f_mc->Get("jet0_pt");
// //       B_mc = h_mc->Integral(-1,h_mc->GetNbinsX(),"width");
// // 	 relf_mc =A_mc*100/B_mc;
// // 	std::cout << "A/B rel-signal--> " << relf_mc << std::endl;
//       
      double M;
	std::cout << "figura de merito s/sqrt D--> " << A_signal/sqrt(A_data) << std::endl;
	M=A_signal/sqrt(A_data);

	
      /////////////////
      // Plot
      /////////////////
      
      //Define de canvas , lagend , and Frame
      TCanvas*  c  = new TCanvas("c","c",600,500);
      TLegend* leg = new TLegend(0.11,0.7,0.45,0.89);
      int counter = 0;
      //a different frame with dimentions, for each kind of trigger  
      double minx=0; double maxx=0;
      if(isjjj){
	if      (trigger_type.at(k)=="single_jet_trigger")   { minx=300; maxx=600;}
	else if (trigger_type.at(k)=="multi_jet_trigger")    { minx=100; maxx=300;}
	else if (trigger_type.at(k)=="single_ht_jet_trigger"){ minx=300; maxx=800;}
      }else{
	if      (trigger_type.at(k)=="single_jet_trigger")   { minx=100; maxx=300;}
      }
      TH2F frame("frame","",minx + maxx,minx,maxx,1,-0.1,1.4);
      
      if  (trigger_type.at(k)=="multi_jet_trigger"){
	frame.GetXaxis()->SetTitle("Third jet #it{p}_{T}^{offline} [GeV]"); 
      }else{ frame.GetXaxis()->SetTitle("Leading jet #it{p}_{T}^{offline} [GeV]");}
      
      frame.SetTitleSize(0.048,"x");
      frame.SetTitleSize(0.048,"y");
      gStyle->SetTitleXOffset(0.95);
      gStyle->SetTitleYOffset(0.95);
      
      frame.GetYaxis()->SetTitle("Efficiency");
      
      frame.Draw("AXIS"); 
      frame.SetTitle("T: "+trigger_name.at(k)+"    T_{0}: "+trigger_prescaled_name.at(k));
      tg->SetMarkerStyle(20);
      tg->SetMarkerSize(0.7);
      tg->Draw("P SAME");
      
      TLine l;
      l.SetLineColor(kRed+1); 
      l.SetLineWidth(2); 
      l.DrawLine(/*trigger_info[ii].Xat99*/x99data, -0.1,/* trigger_info[ii].Xat99*/x99data, 1);
//       tg->SetMarkerColor(kRed+2);
 
      TLine lh;
      lh.SetLineColor(kBlack); 
      lh.SetLineWidth(1); 
      lh.DrawLine(minx, 1, maxx, 1);
      
      TLatex text;
      text.SetTextFont(42);
      text.SetTextSize(0.03);
      text.SetTextColor(kRed+2); 

      text.DrawLatex(minx+140, 1.3-counter*0.06, TString::Format("Xat99: %.2f", /*trigger_info[ii].Xat99*/x99data)); ++counter;
      text.DrawLatex(minx+140, 1.3-counter*0.06, TString::Format("#chi^{2}: %.3f ", tg->GetFunction("fit2")->GetChisquare () )); ++counter;
      counter=1;
      text.SetTextColor(kRed + 4); 
      //       text.DrawLatex(minx+140, 1.2-counter*0.06 , TString::Format("f mc: %.3f "    , relf_mc     )); ++counter;
      text.SetTextColor(kGreen + 2); 
      text.DrawLatex(minx+140, 1.22-counter*0.06 , TString::Format("f signal: %.3f ", relf_signal )); ++counter;
      text.SetTextColor(kRed + 2); 
      text.DrawLatex(minx+140, 1.22-counter*0.06 , TString::Format("f data: %.3f"   , relf_data   )); ++counter;
      text.SetTextColor(kRed + 4); 

      text.DrawLatex(minx+140, 1.22-counter*0.06 , TString::Format("#propto S/ #sqrt{B} %.3f "    , M    )); ++counter;

      //LEG
      leg->Clear();    
      leg->AddEntry(tg, "Data (2017)" ,"p");
      leg->SetFillColor(10); leg->SetBorderSize(0); 
      leg->Draw();
      
      TString outputfile("./Data_plots/factors_");
      outputfile += name ;
      outputfile += "_data17_";
      
      if (isjjj){ outputfile += "jjj.png";} else{outputfile += "gjj.png";}
      c->SaveAs( outputfile); 
      
      
      delete c;delete leg;
      
      
      
 }///End loop over the triggers

    f_data17->Close();f_mc->Close();f_signal->Close();

}



