#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include "TLorentzVector.h"
#include <stdlib.h>
#include <algorithm>

#include <utility>
#include <math.h>

#include <TROOT.h>
#include "TTree.h"
#include "TBranch.h"
#include "TChain.h"
#include "TMath.h"
#include "TFile.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"
#include <sstream>
#include <iomanip>
using namespace std;
TString PATH;
const int nEtaBins = 90;

// Input tree
  TTree *tree;	

// Data
  TString Data = ""; // 2015 or 2016
  bool m_PRW = false;
  bool m_doCleaning=false;
  bool m_MC = true;
  bool m_isjjj= true;
  bool m_data15 = false;
  bool m_data17 = false;

  int  firstTrig = 0; // HLT_j15 (except for 8LC: HLT_j25)

  std::vector<TString> trigger_prescaled_name;
  std::vector<TString> trigger_name;
  std::vector<TString> trigger_type;

//------------
// Histograms
//------------

  TH1D* h_cutflow;
  TH1D* h_all;
  TH1D* h_nocuts;

  TH1D* h_jet0_pt;
  TH1D* h_jet0_eta;
  TH1D* h_jet0_phi;
  TH1D* h_jet1_pt;
  TH1D* h_jet1_eta;
  TH1D* h_jet1_phi;
  TH1D* h_jet2_pt;
  TH1D* h_jet2_eta;
  TH1D* h_jet2_phi;

  std::map<TString, TH1D *> m_pass;

//-----------
// Variables
//-----------

  // GRL
  bool m_applyGRL;
  TString m_GRLxml;

  //configuration variables
  bool m_debug;
  int  m_detailLevel;
  bool m_doData;
  bool m_doPUReweight;
  bool m_useWeighted;
  int  m_step;
  bool m_signal;
  float m_lumi;                   // Lumi we are scaling to
  float m_sampleEvents;           //! MC Events in the sample we are processing
  bool m_onGrid;

  // float cutValue;
  int m_eventCounter;     //!

  TString m_name;
  float m_mcEventWeight;  //!

  float m_ht; //!
  int m_NPV; //!
  float m_actualInteractionsPerCrossing; //!
  float m_averageInteractionsPerCrossing; //!
  int m_runNumber; //!
  Long64_t  m_eventNumber; //!
  int m_lumiBlock; //!

  float m_weight; //!
  float m_weight_xs; //!
  float m_weight_pileup;    //!

  std::vector<float>* m_jet_pt; //!
  std::vector<float>* m_jet_eta; //!
  std::vector<float>* m_jet_phi; //!
  std::vector<float>* m_jet_E; //!
  std::vector<float>* m_jet_Timing; //!
  std::vector<float>* m_jet_GhostMuonSegmentCount; //!
  std::vector<int>*   m_jet_clean_passLooseBad; //!
  std::vector<std::string>* m_passedTriggers; //!

  std::vector<float>*  m_ph_pt; 
  std::vector<float>*  m_ph_phi; 
  std::vector<float>*  m_ph_eta; 
    
  //struct

  struct jet {
    TLorentzVector jet;
    int clean_passLooseBad;
  };
  std::vector<jet> Jets;
    
  //functions

  void fillHistos(const TString & name, const float & jetPt, const float & weight);
  bool passTrig(TString trigName);
  void Initialize();
  void Enable_and_Set_Branches(TChain & tree);
  void BookHistograms();
  void Terminate(void);
  void GetListOfRootFiles ( std::vector< TString> & Files);    
  bool fillControlHistos (const std::vector<jet> & Jets   ,const float & weight);
